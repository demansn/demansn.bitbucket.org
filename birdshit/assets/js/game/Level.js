Game.Level = function(core){
  PIXI.Container.call(this);
  this.core = core;
  this.shit = null;
  this.trunks = [];
  this._leftTrunks = [];
  this._rightTrunks = [];
  this.barriers = [];
  this.pickupObjects = [];
  this.hollows = [];
  this.generateCursor = 0;
  this.currentHeight = 0;
  this.startPosition = 0;
  this._generateTrunkCursor = 1;
  this._trunkCursor = 0;
};

Game.Level.prototype = Object.create(PIXI.Container.prototype);
Game.Level.constructor = Game.Level;

Game.Level.prototype.create = function(){
  this._width = 720;
  this._height = 600;
  this.config = Game.getCurrentLevelConfig()
  this.currentHeight = this.config.height;
  this._generateTrunkCursor = 1;

  this.background = PIXI.Sprite.fromFrame('assets/img/sky_1080_x_3840.png');
  this.background.y = -this.currentHeight;
  this.addChild(this.background);

  this.bird = new Game.Bird();
  this.bird.x = this._width / 2;
  this.bird.y = -this.currentHeight + 350;
  this.addChild(this.bird);

  this.bird.on('giveShit', this._createShit, this);

  this.core.camera.y = this.bird.y - (this.core.camera.height / 2) - 100;
  this.startPosition = this.bird.y;

  this.createStart();
};

Game.Level.prototype.createStart = function(){

  this.startTrunkLeft = PIXI.Sprite.fromFrame(this.config.patch.start.trunks.left);
  this.startTrunkLeft.anchor.x = 0.5;
  this.startTrunkLeft.position.x = 0;
  this.startTrunkLeft.position.y = -this.config.height;
  this.addChild(this.startTrunkLeft);

  this.startTrunkRight = PIXI.Sprite.fromFrame(this.config.patch.start.trunks.right);
  this.startTrunkRight.anchor.x = 0.5;
  this.startTrunkRight.position.x = this._width;
  this.startTrunkRight.position.y = -this.config.height;
  this.addChild(this.startTrunkRight);

  this._trunkCursor = this.startTrunkRight.position.y + this.startTrunkRight.height;
};

Game.Level.prototype.reset = function(){
  this.shit.destroy();
  this.removeChild( this.shit );
  this.shit = null;
  this.core.setFollowObjectCamera(null);
  this.core.camera.y = this.bird.y - (this.core.camera.height / 100 * 20);

  this.generateCursor = 0;

  for(var i = 0; i < this.trunks.length; i += 1){
	this.trunks[i].destroy();
	this.removeChild( this.trunks[i] );
  }

  for(var i = 0; i < this.barriers.length; i += 1){
	this.removeChild(this.barriers[i]);
	this.barriers[i].destroy();
  }

  for(var i = 0; i < this.pickupObjects.length; i += 1){
	this.removeChild(this.pickupObjects[i]);
	this.pickupObjects[i].destroy();
  }

  for(var i = 0; i < this.hollows.length; i += 1){
	this.removeChild(this.hollows[i]);
	this.hollows[i].destroy();
  }

  this.trunks = [];
  this.barriers = [];
  this.pickupObjects = [];
  this.hollows = [];
  this.bird.isTakeShit = false;

  this.currentHeight = this.config.height;
  this._trunkCursor = this.startTrunkRight.position.y + this.startTrunkRight.height;
  this._generateTrunkCursor = 1;
  //this._createTrunks();
};

Game.Level.prototype._createShit = function(){
  this.shit = new Game.Shit();
  this.shit.position.x = this.bird.x;
  this.shit.position.y = this.bird.y;
  this.core.setFollowObjectCamera(this.shit);
  this.addChild(this.shit);
};

// Game.Level.prototype._createTrunks = function(){
//
//   this._leftTrunks = [];
//   this._rightTrunks = [];
//   this.trunks = [];
//
//   var i = 0,
//	   trunksNumber = 10,
//	   startY = -this.currentHeight,
//	   y = startY,
//	   trunk,
//	   dr = 5,
//	   branch, br = 0;
//
//   for(i = 0; i < trunksNumber; i += 1){
//	 trunk = PIXI.Sprite.fromFrame('trunk');
//	 trunk.anchor.x = 0.5;
//	 trunk.position.x = 0;
//	 trunk.position.y = y;
//	 this.addChild(trunk);
//	 this._leftTrunks.push(trunk);
//	 this.trunks.push(trunk);
//
//	 y += trunk.height;
//   }
//
//   y = startY;
//
//   for(i = 0; i < trunksNumber; i += 1){
//	 trunk = PIXI.Sprite.fromFrame('trunk');
//	 trunk.anchor.x = 0.5;
//	 trunk.position.x = this._width;
//	 trunk.position.y = y;
//	 this.addChild(trunk);
//	 this._rightTrunks.push(trunk);
//	 this.trunks.push(trunk);
//
//	 y += trunk.height;
//   }
//
//   this.hollow = new Game.Hollow();
//   this.hollow.x = this._width;
//   this.hollow.y = 1000;
//   this.addChild(this.hollow);
//
//   this.hollows.push(this.hollow);
//
//   this.generateBarrierCursor = 0;
//   this.generateBarriers();
// };

Game.Level.prototype.generateTrunks = function(){
  if(this._trunkCursor < this.core.camera.y + this.core.camera.height){

	var tr = this.config.patch.trunk[this._generateTrunkCursor],
		trunk;

	if(tr && tr.start >= this.currentHeight){
	  while(this._trunkCursor <= this.core.camera.y + this.core.camera.height){

		trunk = PIXI.Sprite.fromFrame(tr.sprite);
		trunk.anchor.x = 0.5;
		trunk.position.x = 0;
		trunk.position.y = this._trunkCursor;
		this.addChild(trunk);
		this.trunks.push(trunk);

		trunk = PIXI.Sprite.fromFrame(tr.sprite);
		trunk.anchor.x = 0.5;
		trunk.position.x = this._width;
		trunk.position.y = this._trunkCursor;
		this.addChild(trunk);
		this.trunks.push(trunk);

		this._trunkCursor += trunk.height;
	  }

	  if(tr.end >= Math.abs(this._trunkCursor) ){
		this._generateTrunkCursor += 1;
	  }
	}


  }
};

Game.Level.prototype.generateBarriers = function(){

  var y = this.generateBarrierCursor,
	  branch, part, branchConfig, Branch;

  if(this.config.parts[this.generateCursor + 1] && this.config.parts[this.generateCursor + 1].start >= this.currentHeight){
	this.generateCursor += 1;
	this.generateBarrierCursor = this.config.parts[this.generateCursor].start;
  }
  part = this.config.parts[this.generateCursor];
//  console.log(this.config.parts[this.generateCursor + 1].start);
  console.log(this.currentHeight);
  //провекра начало генерации участка преград
  if(part && part.start >= this.currentHeight){

	//this.generateBarrierCursor = part.start;
	//генерация участка преград

	while(this.generateBarrierCursor > part.end){

        branchConfig = Game.getBarrierConfig(part.barriers[Game.getRandomInt(0, part.barriers.length - 1)]);
    	  //создание веток
        branch = new Game[branchConfig.constructor](0, -this.generateBarrierCursor, branchConfig, this);

        this.addBarrier(branch);
        this.generateBarrierCursor -= part.deltaPosition + branch.height;

        // for(var j = 0; j < 2; j += 1){
        //   branch = new Game.PickUpObject();
        //   branch.x = Game.getRandomInt(25, this.core.renderer.width - 25);
        //   branch.y = Game.getRandomInt(this.generateBarrierCursor - 240, this.generateBarrierCursor - 10);
        //   this.addPickupObject(branch);
        // }
    	}
  }

};

Game.Level.prototype.moveThroughHollow = function(right){
  if(this.shit.inputEnabled){
	this.shit.inputEnabled = false;

	var tl = new TimelineMax({
	  onComplete: function(){
		this.shit.inputEnabled = true;
	  },
	  onCompleteScope: this
	});

	tl.to(this.core.camera, 1, {x: this.core.camera.x - this.core.camera.width, ease:Power2.easeOut})
	  .to(this.shit.position, 0.8, {x: (this._width + (this._width / 2)), ease:Power2.easeOut}, 0.2)
	  .to(this.shit.position, 0.7, {y: this.shit.y + 250, ease:Power2.easeIn}, 0.3);
  }
};

Game.Level.prototype.addBarrier = function(barrier){
	this.barriers.push(barrier);
	this.addChild(barrier);
};

Game.Level.prototype.addPickupObject= function(pickup){
  this.pickupObjects.push(pickup);
  this.addChild(pickup);
};

Game.Level.prototype.createFinish = function(){
  var trunk;

  this.ground = PIXI.Sprite.fromFrame(this.config.assets.ground);
  this.ground.y = -this.ground.height;
  this.ground.x = this._width * this.config.patch.end.ground.x;

  this.addChild(this.ground);

  trunk = PIXI.Sprite.fromFrame(this.config.patch.end.tranks.left);
  trunk.anchor.x = 0.5;
  trunk.position.x = 0;
  trunk.position.y = -trunk.height;
  this.addChild(trunk);
  this.trunks.push(trunk);

  trunk = PIXI.Sprite.fromFrame(this.config.patch.end.tranks.right);
  trunk.anchor.x = 0.5;
  trunk.position.x = this._width;
  trunk.position.y = -trunk.height;
  this.addChild(trunk);
  this.trunks.push(trunk);

};

Game.Level.prototype.update = function(){
  var newArr = [];

  if(this.shit){
	this.shit.update();
	this.currentHeight = Math.abs(this.core.camera.y + this.core.camera.height);

	this.generateBarriers();
	this.generateTrunks();

	if(this.currentHeight - this.core.camera.height <= 0 && !this.ground){
	  this.createFinish();
	}

	if(this.core.camera.y + (this.core.camera.height / this.core.worldLayer.scale.y) >= 0){
	  this.core.setFollowObjectCamera(null);
	}

	if(this.shit.y + this.shit.height >= 0){
	  this.shit.y = -this.shit.height;
	  this.shit.inputEnabled = false;
	}

	for(var i = 0; i < this.trunks.length; i += 1){
	  if(this.trunks[i].y + this.trunks[i].height < this.core.camera.y){
		this.trunks[i].parent.removeChild(this.trunks[i]);
		this.trunks[i].destroy();
	  } else {
		newArr.push(this.trunks[i]);
	  }
	}

	this.trunks = newArr;
	newArr = [];

	for(var i = 0; i < this.barriers.length; i += 1){

		if(this.barriers[i].update){
			this.barriers[i].update();
			this.barriers[i].debug();
		}

    	if(this.barriers[i].y + this.barriers[i].height < this.core.camera.y){
    		this.barriers[i].parent.removeChild(this.barriers[i]);
    		this.barriers[i].destroy();
    	} else {
    		newArr.push(this.barriers[i]);
    	}
	}
	this.barriers = newArr;
  }

};

Game.Level.prototype._replayTrunks = function(trunks){
  var id, trunk;

  for(var i = 0; i < trunks.length; i += 1){
	trunk = trunks[i];
	if(trunk.y + trunk.height < this.core.camera.y){
	  trunk.y = trunks[trunks.length - 1].y + trunks[trunks.length - 1].height;
	}
  }

  trunks.sort(function(a, b){
	if(a.y < b.y){
	   return -1 // Или любое число, меньшее нуля
	} else if(a.y > b.y){
	   return 1  // Или любое число, большее нуля
	}
	return 0
  });
}
