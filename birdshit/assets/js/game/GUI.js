Game.GUI = function(core){
  PIXI.Container.call(this);
  var preloadBgr, loadingText;

  this.core = core;

  this.gameUI = new PIXI.Container();
  this.gameUI.visible = false;
  this.addChild(this.gameUI);

  this.preloadLayer = new PIXI.Container();
  this.addChild(this.preloadLayer);

  preloadBgr = new PIXI.Graphics();
  preloadBgr.beginFill(0xffffff, 1);
  preloadBgr.drawRect(0, 0, this.core.renderer.width, this.core.renderer.height);
  this.preloadLayer.addChild(preloadBgr);

  loadingText = new PIXI.Text('Загрузка...', { font: '50px Snippet', fill: 'green', align: 'center' });
  loadingText.position.set(this.core.renderer.width / 2, this.core.renderer.height / 100 * 65);
  loadingText.anchor.set(0.5, 0.5);
  this.preloadLayer.addChild(loadingText);

};

Game.GUI.prototype = Object.create(PIXI.Container.prototype);
Game.GUI.constructor = Game.GUI;

Game.GUI.prototype.init = function(){

    this.mainMenu = new Game.MainMenu();
    this.addChild(this.mainMenu);
  this.power = new PIXI.Graphics();
  this.power.beginFill(0x00ff00, 1);
  this.power.drawRect(0, 0, this.core.renderer.width, 10);

  this.heightView = new PIXI.extras.BitmapText('10000', { font: '35px 3-export', align: 'right' });

  this.heightView.position.x = this.core.renderer.width - this.heightView.width;
  this.heightView.position.y = 20;
  this.gameUI.addChild( this.heightView);

  this.gameUI.addChild( this.power );

  this.preloadLayer.visible = false;
};

Game.GUI.prototype.showMainMenu = function(){
    this.mainMenu.show();
};

Game.GUI.prototype.hideMainMenu = function(){
    this.mainMenu.hide();
};

Game.GUI.prototype.showGameUI = function(){
  this.gameUI.visible = true;
};

Game.GUI.prototype.hideGameUI = function(){
  this.gameUI.visible = false;
};

Game.GUI.prototype.updateGameUI = function(level){
  this.power.clear();

  this.power.beginFill(0x00ff00, 1);
  this.power.drawRect(0, 0, this.core.renderer.width / 100 * level.shit.health, 10);
  this.heightView.text = Math.round(level.shit.y / 100, 10);
};
