Game.Branch = function(x, y, config, level){
  PIXI.Container.call(this);

  this.x = x;
  this.y = y;
  this.config = config;
  this.parts = [];
  this.rects = [];

  var part, partConfig, id, graphics,
      rect = {};

  for(id in this.config.parts){

    partConfig = this.config.parts[id];
    // part = PIXI.Sprite.fromFrame(partConfig.sprite);
    // part.x = level._width * partConfig.x;
    // part.y = partConfig.y;
    rect = {
        delataPostion: (level._width * partConfig.x),
      x: this.x + (level._width * partConfig.x) + partConfig.rectangle.x,
      y: this.y + partConfig.rectangle.y,
      width: partConfig.rectangle.width,
      height:  partConfig.rectangle.height
    };

    if(partConfig.right){
      rect.x -= rect.width;
    }

    graphics = new PIXI.Graphics();
    graphics.id = id;
    graphics.rect = rect;
    graphics.config = partConfig;
    graphics.beginFill(0x00FF00, 0.5);
//    graphics.drawRect(rect.x, rect.y, rect.width, rect.height);
    graphics.alpha = 1;

    this.addChild(graphics);

  }

  this.updateRectPostion();
};

Game.Branch.prototype = Object.create(PIXI.Container.prototype);
Game.Branch.constructor = Game.Branch;

Game.Branch.prototype.getPartById = function(id){

  for(var i = 0; i < this.children.length; i += 1){
    if(this.children[i].id === id){
      return this.children[i];
    }
  }

  return null;
};

Game.Branch.prototype.isCollision = function(shit){
  var collision = false,
      halfWidthA = shit.width / 2,
      halfHeightA = shit.height / 2,
      distX = 0,
      distY = 0,
      collX = false,
      collY = false,
      part, i;

  for(i = 0; i < this.children.length; i += 1){
    part = this.children[i].rect;
    distX = part.x - shit.position.x;
    distY = part.y - shit.position.y;
    collX = false;
    collY = false;

    if(distX <= halfWidthA){
      if(distX + part.width >= -halfWidthA){
        collX = true;
      }
    }

    if(distY <= halfHeightA){
      if(distY + part.height >= -halfHeightA){
        collY = true;
      }
    }

    collision = collX && collY;

    if(collision){
      break;
    }
      // if(shit.position.y - halfHeightA > part.y + this.position.y && shit.position.y + halfHeightA < part.y + this.position.y + part.height){
      //   collision = true;
      // }
    //}
  }



  return collision;
};

Game.Branch.prototype.updateRectPostion = function(){
    for(i = 0; i < this.children.length; i += 1){
        this.children[i].rect.x = this.x + this.children[i].position.x + this.children[i].rect.delataPostion;
        this.children[i].rect.y = this.y + this.children[i].position.y;

        if(this.children[i].config.right){
          this.children[i].rect.x -= this.children[i].rect.width;
        }
    }
};

Game.Branch.prototype.update = function(){
    //this.updateRectPostion();
};

Game.Branch.prototype.debug = function(shit){
    for(i = 0; i < this.children.length; i += 1){
        Game.core.drawRect(this.children[i].rect);
    }
};
