Game.Hollow = function(){
  PIXI.Container.call(this);

  this.view =  PIXI.Sprite.fromFrame('hollow');
  this.view.anchor.set(0.5, 0.5);
  this.addChild(this.view);
};

Game.Hollow.prototype = Object.create(PIXI.Container.prototype);
Game.Hollow.constructor = Game.Hollow;
