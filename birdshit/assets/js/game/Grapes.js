Game.Grapes = function(x, y, config, level){
	Game.Branch.call(this, x, y, config, level);

	this.leftPart = this.getPartById('left');
	this.rightPart = this.getPartById('right');
};

Game.Grapes.prototype = Object.create(Game.Branch.prototype);
Game.Grapes.constructor = Game.Grapes;

Game.Grapes.prototype.startMove = function(){
	this.startedMove = true;
};

Game.Grapes.prototype.update = function(){

	var endPos;

	if(this.startedMove){

		endPos = this.leftPart.position.x + this.leftPart.config.deltaPostion;

		if(this.leftPart.position.x !== endPos){
			this.leftPart.position.x += this.leftPart.config.speed;

			if(this.leftPart.position.x > endPos){
				this.leftPart.position.x = endPos;
			}

			//this.leftPart.rect.x = this.leftPart.position.x;
		}

		endPos = this.rightPart.position.x - this.rightPart.config.deltaPostion;

		if(this.rightPart.position.x !== endPos){
			this.rightPart.position.x -= this.rightPart.config.speed;

			if(this.rightPart.position.x < endPos){
				this.rightPart.position.x = endPos;
			}

		//	this.rightPart.rect.x = this.rightPart.position.x;
		}

	} else {
		if(this.y >= Game.core.camera.y + Game.core.camera.height){
			this.startMove();
		}
	}
	this.updateRectPostion();
};
