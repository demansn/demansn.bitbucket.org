Game.Core = function(){
  this.container = document.getElementById(Game.containerId);
  this.gameWidth = Game.width;
  this.gameHeight = Game.height;
  this.viewWidth = this.container.clientWidth;
  this.viewHeight = this.container.clientHeight;
  this.renderer = PIXI.autoDetectRenderer(this.viewWidth,   this.viewHeight);
  this.container.appendChild(this.renderer.view);
  this.stage = new PIXI.Container();
  this.worldLayer = new PIXI.Container();
  this.guiLayer = new PIXI.Container();
  this.debugLayer = new PIXI.Graphics();

  this.stage.addChild(this.worldLayer);
  this.stage.addChild(this.guiLayer);
  this.worldLayer.addChild(this.debugLayer);

  this.loader = PIXI.loader;
  this.camera = new PIXI.Point();
  this.camera.deadZone = {x: 0, y: 20, width: this.renderer.width, height: this.renderer.height / 100 * 85};
  this.camera.width = this.renderer.width;
  this.camera.height = this.renderer.height;

  this.collisionManager = new Game.CollisionManager(this);

  requestAnimationFrame(this.render.bind(this));
  this.initSize();
};

Game.Core.prototype.preload = function() {
  var atlas = Game.config.resources.atlas,
      imgs = Game.config.resources.imgs,
      id;

  for(id in atlas){
      this.loader.add(atlas[id]);
  }

  this.loader.add('barriers-config','./assets/config/barriers.json', {loadType: 'json'});
  this.loader.add('level1-config','./assets/config/level1.json', {loadType: 'json'});
  this.loader.add('3-export', 'assets/bitmapFonts/3-export.xml');
  this.loader.add('bgrMainMenu', 'assets/img/brgMainMenu.png');
  this.loader.add('bgrSelectLevel', 'assets/img/brgSelectLevel.png');
  this.loader.add('sky_1080_x_3840', 'assets/img/sky_1080_x_3840.png');
  //this.loader.add(imgs);

  this.loader.load(this.onLoaded.bind(this));

  this.loader.add('crow', 'assets/spine/100/crow100.json');
};

Game.Core.prototype.onLoaded = function(loader, res) {
  Game.config['barriers'] = res['barriers-config'].data;
  Game.config['level1'] = res['level1-config'].data;
  Game.start();
};

Game.Core.prototype.render = function() {
  requestAnimationFrame(this.render.bind(this));
  this.debugLayer.clear();
  Game.update();
  this.collisionManager.update();

  if(this._followObjectCamera){

    var foy = this._followObjectCamera.position.y;
    if(foy > this.camera.y + (this.camera.height - this.camera.deadZone.height)){
      this.camera.y -= (this.camera.y - foy - (this.camera.height - this.camera.deadZone.height)) / 50;
     // this.camera.y = foy - (this.camera.height - this.camera.deadZone.height);
    } else if(foy < this.camera.y - this.camera.deadZone.y){
    //  this.camera.y = foy - this.camera.deadZone.y;
    }
  }

  this.worldLayer.position.set(this.camera.x * this.worldLayer.scale.x, -this.camera.y * this.worldLayer.scale.y);

  this.renderer.render(this.stage);
};

Game.Core.prototype.setFollowObjectCamera = function(object) {
  this._followObjectCamera = object;
};

Game.Core.prototype.drawRect = function(rect){
    this.debugLayer.beginFill(0x00FF00, 0.5);
    this.debugLayer.drawRect(rect.x, rect.y, rect.width, rect.height);
};

Game.Core.prototype.initSize = function() {
  //N=Высота_экрана/Высота_Спрайта.
  this.worldLayer.scale.x = this.worldLayer.scale.y = this.renderer.width / this.gameWidth;

};
