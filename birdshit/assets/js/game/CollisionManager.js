
Game.CollisionManager = function(engine){
	this.engine = engine;
};

Game.CollisionManager.constructor = Game.CollisionManager;

Game.CollisionManager.prototype.update = function(){
	if(Game.isPlaying && Game.level.shit !== null && Game.level.shit.inputEnabled){
		if(!this.playerVsHollow()){
			this.playerVsTrunks();
			this.playerVsBrunch();
			this.playerVsPickup();
			this.playerVsGround();
		}
	}
};

Game.CollisionManager.prototype.playerVsHollow = function() {

	var hollows = Game.level.hollows,
			shit = Game.level.shit,
			halfWidthA = Game.level.shit.width / 2,
			halfHeightA = Game.level.shit.height / 2,
			halfWidthB = 0,
			halfHeightB = 0,
			hollow, i, xdist,
			coll = false;

	for (i = 0; i < hollows.length; i++) {
		hollow = hollows[i]

		xdist = hollow.position.x - shit.position.x;
		ydist = hollow.position.y - shit.position.y;
		halfWidthB = hollow.width / 2;
		halfHeightB = hollow.height;

		if( xdist + halfWidthA > -halfWidthB && xdist - halfWidthA < halfWidthB ) {
			if( ydist + halfHeightA > -halfHeightB && ydist - halfHeightA < halfHeightB ) {
				Game.level.moveThroughHollow();
				coll = true;
				break;
			}
		}
	}

	return coll;
};

Game.CollisionManager.prototype.playerVsTrunks = function() {

	var trunks = Game.level.trunks,
			shit = Game.level.shit,
			halfWidthA = Game.level.shit.width / 2,
			halfHeightA = Game.level.shit.height / 2,
			halfWidthB = 0,
			halfHeightB = 0,
			trunk, i, xdist;

	for (i = 0; i < trunks.length; i++) {
		trunk = trunks[i]

		xdist = trunk.position.x - shit.position.x;
		ydist = trunk.position.y - shit.position.y;
		halfWidthB = trunk.width / 2;
		halfHeightB = trunk.height;

		if( xdist + halfWidthA > -halfWidthB && xdist - halfWidthA < halfWidthB ) {
			if( shit.position.y - halfHeightA > trunk.position.y && shit.position.y + halfHeightA < trunk.position.y + trunk.height ) {
				Game.over();
				break;
			}
		}
	}
};

Game.CollisionManager.prototype.playerVsBrunch = function() {

	var barriers = Game.level.barriers,
			shit = Game.level.shit,
			i = 0;

			for(i = 0; i < barriers.length; i += 1){
				if(barriers[i].isCollision(shit)){
					Game.over();
					break;
				}
			}
};

Game.CollisionManager.prototype.playerVsPickup = function() {

	if( !Game.level.shit ){
		return;
	}
	var shit = Game.level.shit,
			pickupObjects = Game.level.pickupObjects,
			collision = false,
      halfWidthA = shit.width / 2,
      halfHeightA = shit.height / 2,
      distX = 0,
      distY = 0,
      collX = false,
      collY = false,
      pickup, i;

  for(i = 0; i < pickupObjects.length; i += 1){
    pickup = pickupObjects[i];
    distX = pickup.x - shit.position.x;
    distY = pickup.y - shit.position.y;
    collX = false;
    collY = false;

		if(pickup.value <= 0){
			continue;
		}

    if(distX <= halfWidthA){
      if(distX + pickup.width >= -halfWidthA){
        collX = true;
      }
    }

    if(distY <= halfHeightA){
      if(distY + pickup.height >= -halfHeightA){
        collY = true;
      }
    }

    collision = collX && collY;

    if(collision){
			Game.level.shit.addHealth(pickup.value);
			pickup.value = 0;
			pickup.visible = false;
    }
  }

};

Game.CollisionManager.prototype.playerVsGround = function() {

	if(!Game.level.ground || !Game.level.shit ){
		return false;
	}

	var collision = false,
			ground = Game.level.ground,
			shit = Game.level.shit,
      halfWidthA = shit.width / 2,
      halfHeightA = shit.height / 2,
      distX = 0,
      distY = 0,
      collX = false,
      collY = false,
      part, i;

    distX = ground.x - shit.position.x;
    distY = ground.y - shit.position.y;
    collX = false;
    collY = false;

    if(distX <= halfWidthA){
      if(distX + ground.width >= -halfWidthA){
        collX = true;
      }
    }

    if(distY <= halfHeightA){
      if(distY + ground.height >= -halfHeightA){
        collY = true;
      }
    }

		if(collX && collY){
				Game.over();
				return true;
		}

		return false;
};
