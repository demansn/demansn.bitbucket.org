Game.LevelSelectionPanel = function(){
    PIXI.Container.call(this);

    this.bgr = PIXI.Sprite.fromFrame('assets/img/brgSelectLevel.png');
    this.addChild(this.bgr);

    this._items = {};

    this.selectionOne = PIXI.Sprite.fromFrame('icon_level1');
    this.selectionOne.anchor.set(0.5, 0.5);
    this.selectionOne.position.set(Game.core.renderer.width / 2, Game.core.renderer.height / 100 * 20);
    this.selectionOne.interactive = true;
    this.selectionOne
        .on('mousedown', this._onClickLevel, this)
        .on('touchstart', this._onClickLevel, this);
    this.selectionOne.id = 'level1';
    this.addChild(this.selectionOne);
    this._items[this.selectionOne.id] = this.selectionOne;

    this.selectionTwo = PIXI.Sprite.fromFrame('icon_level2');
    this.selectionTwo.anchor.set(0.5, 0.5);
    this.selectionTwo.position.set(Game.core.renderer.width / 2, Game.core.renderer.height / 100 * 50);
    this.selectionTwo.interactive = true;
    this.selectionTwo
        .on('mousedown', this._onClickLevel, this)
        .on('touchstart', this._onClickLevel, this);
    this.selectionTwo.id = 'level2';
    this.addChild(this.selectionTwo);
    this._items[this.selectionTwo.id] = this.selectionTwo;

    this.selectionThree = PIXI.Sprite.fromFrame('icon_level3');
    this.selectionThree.anchor.set(0.5, 0.5);
    this.selectionThree.position.set(Game.core.renderer.width / 2, Game.core.renderer.height / 100 * 70);
    this.selectionThree.interactive = true;
    this.selectionThree
        .on('mousedown', this._onClickLevel, this)
        .on('touchstart', this._onClickLevel, this);
    this.selectionThree.id = 'level3';
    this.addChild(this.selectionThree);
    this._items[this.selectionThree.id] = this.selectionThree;

    this.closeBtn = new Game.Button('closeBtnNormal', 'closeBtnPress');
    this.closeBtn.anchor.set(0.5, 0.5);
    this.closeBtn.position.set(Game.core.renderer.width / 100 * 10, Game.core.renderer.height / 100 * 10)
    this.closeBtn.on('onClick', this.hide, this);
    this.addChild(this.closeBtn);

    this.visible = false;
};

Game.LevelSelectionPanel.prototype = Object.create(PIXI.Container.prototype);
Game.LevelSelectionPanel.constructor = Game.LevelSelectionPanel;

Game.LevelSelectionPanel.prototype._onClickLevel = function(e){
    this.selectItem(e.target.id);
};

Game.LevelSelectionPanel.prototype.selectItem = function(id){
    if(this._currentSelection){
        this._currentSelection.scale.set(1, 1);
    }

    if(this._items[id]){
        this._currentSelection = this._items[id];
        this._currentSelection.scale.set(1.1, 1.1);

        this.emit('onSelect', id);
    }
};

Game.LevelSelectionPanel.prototype.show = function(){
    this.visible = true;
};

Game.LevelSelectionPanel.prototype.hide = function(){
    this.visible = false;
};
