Game.Shit = function(){
  PIXI.Sprite.call(this);
  this.texture = PIXI.Texture.fromFrame("shit");
  this.anchor.set(0.5, 0.5);

  if(typeof Cocoon !== 'undefined'){
    Cocoon.Touch.enable();
    window.addEventListener('devicemotion', this._updatePositionXByDeviceMotion.bind(this), true);
  } else {
    this.interactive = true;
    //this.on('mousemove', this._updatePositionXByMouse, this)
    Game.core.renderer.view.addEventListener('mousemove', this._updatePositionXByMouse.bind(this));
  }

  this.speed = 3;
  this.health = 100;
  this.deltaHealth = 0.05;
  this.inputEnabled = true;
  this.moveDirection = 0;
}

Game.Shit.prototype = Object.create(PIXI.Sprite.prototype);
Game.Shit.constructor = Game.Shit;

Game.Shit.prototype._updatePositionXByMouse = function(e){
  if(this._inputEnabled){
    var center = Game.core.camera.width / 2,
        position = e.layerX,
        proc = 0, speed = 0.07;
    if(position < center){
        proc = 100 - (position / center * 100);
        this.moveDirection = -speed * proc;
    } else if(position > center){
        proc = ((position - center) / center * 100);
        this.moveDirection = speed * proc;
    } else {
        this.moveDirection = 0;
    }
  }
};

Game.Shit.prototype._updatePositionXByDeviceMotion = function(e){
  var ac = e.acceleration.x;

  if(this._inputEnabled){
    if( Math.abs(ac) > 0.11 ){
      this.moveDirection = -ac
    } else {
      this.moveDirection = 0;
    }
  }
};

Game.Shit.prototype.addHealth = function(dh){
  this.health += dh;

  if(this.health > 100){
    this.health = 100;
  }
};

Game.Shit.prototype.update = function(){
  if(this._inputEnabled){
    this.position.y += this.speed + (this.speed / 100 * (100 - this.health));
    this.position.x += this.moveDirection * 2;

    if(this.health > 0){
      this.health -= this.deltaHealth;
    }
  }
};

Object.defineProperty(Game.Shit.prototype, 'inputEnabled', {
  set: function(value){
    this._inputEnabled = value;
  },
  get: function(){
    return this._inputEnabled;
  }
});
