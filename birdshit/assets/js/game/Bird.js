Game.Bird = function(){
  PIXI.Sprite.call(this);
  this.texture = PIXI.Texture.fromFrame("bird");
  this.anchor.set(0.5, 1);



  this.isTakeShit = false;

  this.animation = new PIXI.spine.Spine(PIXI.loader.resources.crow.spineData);
  this.animation.state.setAnimationByName(0, 'stay', true);
  this.animation.interactive = true;

  this.animation
            .on('mouseup', this._onUp, this)
            //.on('touchstart', this._giveShit, this)
            .on('touchend', this._onUp, this)
               // set the mousedown and touchstart callback...
            .on('mousedown', this._onDown, this)
            .on('touchstart', this._onDown, this)

               // set the mouseup and touchend callback...


               // set the mouseover callback...
               //.on('mouseover', onButtonOver)

               // set the mouseout callback...
              // .on('mouseout', onButtonOut)
          //this.tap = this._giveShit.bind(this);

  this.addChild( this.animation );

}

Game.Bird.prototype = Object.create(PIXI.Sprite.prototype);
Game.Bird.constructor = Game.Bird;

Game.Bird.prototype._giveShit = function(){
  if(!this.isTakeShit){
    this.emit('giveShit');
    this.isTakeShit = true;
  }
};

Game.Bird.prototype._onUp = function(){
    this.animation.state.setAnimationByName(0, 'action', false);
    this.animation.state.addAnimationByName(0, 'stay', true, 0);
    this._giveShit();
};

Game.Bird.prototype._onDown = function(){
    this.animation.state.setAnimationByName(0, 'start_heave');
    this.animation.state.addAnimationByName(0, 'heave', true, 0);
};
