Game.MainMenu = function(){
    PIXI.Container.call(this);
    this.bgr = PIXI.Sprite.fromFrame('assets/img/brgMainMenu.png');
    this.addChild(this.bgr);

    this.btnPlay = new Game.Button('btnNormal', 'btnPress');
    this.btnPlay.anchor.set(0.5, 0.5);
    this.btnPlay.position.set(Game.core.renderer.width / 2, Game.core.renderer.height / 100 * 15);
    this.addChild(this.btnPlay);

    this.btnLevels = new Game.Button('btnNormal', 'btnPress');
    this.btnLevels.anchor.set(0.5, 0.5);
    this.btnLevels.position.set(Game.core.renderer.width / 2, Game.core.renderer.height / 100 * 45);
    this.btnLevels.on('onClick', this.showLevelSelectionPanel, this);
    this.addChild(this.btnLevels);

    this.btnToggleSound = new Game.ToggleButton('toggleBtnEnabled', 'toggleBtnDisabled');
    this.btnToggleSound.anchor.set(0.5, 0.5);
    this.btnToggleSound.position.set(Game.core.renderer.width / 100 * 85, Game.core.renderer.height / 100 * 10);
    this.btnToggleSound.on('toggle', this._onToggleSoundBtn, this);
    this.addChild(this.btnToggleSound);

    this.btnToggleMusic = new Game.ToggleButton('toggleBtnEnabled', 'toggleBtnDisabled');
    this.btnToggleMusic.anchor.set(0.5, 0.5);
    this.btnToggleMusic.position.set(Game.core.renderer.width / 100 * 85, Game.core.renderer.height / 100 * 25);
    this.btnToggleMusic.on('toggle', this._onToggleMusicBtn, this);
    this.addChild(this.btnToggleMusic);

    this.levelsMenu = new Game.LevelSelectionPanel();
    this.levelsMenu.on('onSelect', this.selectLevel, this);
    this.addChild(this.levelsMenu);

    this.levelsMenu.selectItem(Game.getData('currentLevel'));
};

Game.MainMenu.prototype = Object.create(PIXI.Container.prototype);
Game.MainMenu.constructor = Game.MainMenu;

Game.MainMenu.prototype.show = function(){
    this.visible = true;
};

Game.MainMenu.prototype.hide = function(){
    this.visible = false;
};

Game.MainMenu.prototype._onToggleSoundBtn = function(enabled){
    this.emit('onToggleSoundBtn', enabled);
    console.log(enabled);
};

Game.MainMenu.prototype._onToggleMusicBtn = function(enabled){
    this.emit('onToggleMusicBtn', enabled);
    console.log(enabled);
};

Game.MainMenu.prototype.showLevelSelectionPanel = function(){
    this.levelsMenu.show();
};

Game.MainMenu.prototype.selectLevel = function(id){
    Game.saveData('currentLevel', id);
};
