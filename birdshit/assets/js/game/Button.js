Game.Button = function(normalFrameId, pressFrameId){
    this.normalTexture = PIXI.Texture.fromFrame(normalFrameId);
    this.pressTexture = PIXI.Texture.fromFrame(pressFrameId);
    PIXI.Sprite.call(this, this.normalTexture);

    this.interactive = true;
    this
      .on('mousedown', this.onButtonDown, this)
      .on('touchstart', this.onButtonDown, this)
      .on('mouseup', this.onButtonUp, this)
      .on('touchend', this.onButtonUp, this)
      .on('mouseupoutside', this.onButtonUp, this)
      .on('touchendoutside', this.onButtonUp, this)
      .on('mouseout', this.onButtonOut, this);
};

Game.Button.prototype = Object.create(PIXI.Sprite.prototype);
Game.Button.constructor = Game.Button;

Game.Button.prototype.onButtonDown = function(){
    this.isDown = true;
    this.texture = this.pressTexture;
};
Game.Button.prototype.onButtonUp = function(){

    if(this.isDown){
        this.isDown = false;
        this.texture = this.normalTexture;
        this.emit('onClick');
    }
};

Game.Button.prototype.onButtonOut = function(){
    if(this.isDown){
        this.isDown = false;
        this.texture = this.normalTexture;
    }
};
