Game.ToggleButton = function(enabledStateFrameId, disabledStateFrameId){
    this.enabledStateTexture = PIXI.Texture.fromFrame(enabledStateFrameId);
    this.disabledStateTexture = PIXI.Texture.fromFrame(disabledStateFrameId);
    PIXI.Sprite.call(this, this.enabledStateTexture);

    this.interactive = true;
    this
      .on('mousedown', this.onButtonDown, this)
      .on('touchstart', this.onButtonDown, this)
      .on('mouseup', this.onButtonUp, this)
      .on('touchend', this.onButtonUp, this)
      .on('mouseupoutside', this.onButtonUp, this)
      .on('touchendoutside', this.onButtonUp, this)
      .on('mouseout', this.onButtonOut, this);
    this._enabled = true;
};

Game.ToggleButton.prototype = Object.create(PIXI.Sprite.prototype);
Game.ToggleButton.constructor = Game.ToggleButton;

Game.ToggleButton.prototype.onButtonDown = function(){
    this.isDown = true;
};
Game.ToggleButton.prototype.onButtonUp = function(){

    if(this.isDown){
        this.isDown = false;
        this.enabled = !this.enabled;
    }
};

Game.ToggleButton.prototype.onButtonOut = function(){
    if(this.isDown){
        this.isDown = false;
    }
};

Object.defineProperty(Game.ToggleButton.prototype, 'enabled', {
    set: function(value){
        if(value !== this._enabled){
            this._enabled = value;

            if(this._enabled){
                this.texture = this.enabledStateTexture;
            } else {
                this.texture = this.disabledStateTexture;
            }

            this.emit('toggle', this._enabled);
        }
    },
    get: function(){
        return this._enabled;
    }
});
