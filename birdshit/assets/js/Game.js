var Game = {};
Game.width = 720;
Game.height = 1280;
Game.containerId = 'gameContainer';

Game.init = function(){
  this.container = document.getElementById(this.containerId);
  this.viewWidth = this.container.innerWidth;
  this.viewHeight = this.container.innerHeight;
  this.core = new Game.Core();
  this.level = new Game.Level(this.core);
  this.gui = new Game.GUI(this.core);
  this.core.worldLayer.addChild(this.level);
  this.core.guiLayer.addChild(this.gui);
  this.core.preload();
};

Game.start = function(){
  this.gui.init();
  this.gui.mainMenu.btnPlay.on('onClick', this.startGame, this);
  this.level.create();
  this.isPlaying = true;
  this.gui.showMainMenu();
};

Game.startGame = function(){
    this.gui.hideMainMenu();
    this.gui.showGameUI();
};

Game.over = function(){
  this.level.reset();
};

Game.pause = function(){
  this.level.shit.speed = 0;
};

Game.update = function(){
  this.level.update();
  if(this.level.shit){
    this.gui.updateGameUI(this.level);
  }
};

Game.getCurrentLevelConfig = function(){

    var id = localStorage['currentLevel'] || 'level1',
        config = Game.config[id] || Game.config['level1'];

    return config;
};

Game.addChildToStage = function(child){
  this.core.worldLayer.addChild(child);
};

Game.getBarrierConfig = function(id){
  return Game.config['barriers'][id];
};

Game.getRandomInt = function(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

Game.saveData = function(key, data){
    localStorage.setItem(key, JSON.stringify(data));
};

Game.getData = function(key){
    return JSON.parse(localStorage.getItem(key) || 'null');
};
