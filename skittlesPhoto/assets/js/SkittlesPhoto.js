var SkittlesPhoto = {};

SkittlesPhoto.LOAD_PHOTO = 0;
SkittlesPhoto.TRANSFORM = 1;
SkittlesPhoto.ROTATE = 2
SkittlesPhoto.TRANSLATE = 3;
SkittlesPhoto.defaultPhoto = null;
SkittlesPhoto.debug = false;
SkittlesPhoto.currentMode = null;

SkittlesPhoto.init = function(container, width, height){

	this.$preloadBar = $('#preloadBar');
	this.$editor = $('#editor');
	this.$takePhotoInput = $('#takePhotoBtnBgr, #takePhotoBtnInput');

	if(SkittlesPhoto.debug){
		this.$takePhotoInput.hide();
	}

	$('.modeBtn').click(SkittlesPhoto._onClickMode.bind(this));
	$('#takePhotoBtnInput').click(SkittlesPhoto._onClickMode.bind(this));

	this.localPhotoReader = new SkittlesPhoto.PhotoReader();

	this.game = new Phaser.Game('100%', '100%', Phaser.CANVAS, 'photoContainer');

	this.game.state.add('Boot', SkittlesPhoto.Boot);
	this.game.state.add('PreloadPhoto', SkittlesPhoto.PreloadPhoto);
	this.editor = this.game.state.add('Editor', SkittlesPhoto.Editor);

	this.game.state.start('Boot');

	//this.preloadBarVisible = false;
	document.getElementById('takePhotoBtnInput').addEventListener('change', function(e){
		//выбор фото пользователем
		//начать загрузку фотографии на клиент
		SkittlesPhoto.loadPhotoToClient(e);
	}, false);
};

SkittlesPhoto._onClickMode = function(e) {
	switch(e.target.id){
		case 'modeBtnTransform':
			this.setMode(SkittlesPhoto.TRANSFORM);
			break;
	}
};

SkittlesPhoto.setMode = function(modeId) {

	if(SkittlesPhoto.currentMode === modeId){
		return;
	}

	if(this.editor){
		SkittlesPhoto.currentMode = modeId;
		this.editor.setMode(modeId);
	}
};

SkittlesPhoto.addDecal = function(decalId) {
	console.log('add decal - ' + decalId);
};

SkittlesPhoto.loadPhotoToClient = function(e) {

	if(e.target.files.length > 0) {
		this.localPhotoReader.once('onLoad', SkittlesPhoto.addPhoto, this);
		this.localPhotoReader.loadPhoto(e.target.files[0]);
		this.preloadBarVisible = true;

	} else {
		console.log('Файл не выбран');
	}

};

SkittlesPhoto.addPhoto = function(e) {

	this.game.load.image('photo', e.target.result);
	this.game.load.onLoadComplete.addOnce(function(){
		this.editor.addPhoto();
		SkittlesPhoto.preloadBarVisible = false;
		SkittlesPhoto.setMode('');
	}, this);

	this.game.load.start();

};

SkittlesPhoto.selectBtnA = function() {
	switch(SkittlesPhoto.currentMode){
		case SkittlesPhoto.TRANSLATE:
			break;
		case SkittlesPhoto.TRANSFORM:
			this.editor.scalePhoto(0.1);
			break;
		case SkittlesPhoto.ROTATE:
			this.editor.rotatePhoto(1);
			break;
	}
};

SkittlesPhoto.selectBtnB = function() {
	switch(SkittlesPhoto.currentMode){
		case SkittlesPhoto.TRANSLATE:
			break;
		case SkittlesPhoto.TRANSFORM:
			this.editor.scalePhoto(-0.1);
			break;
		case SkittlesPhoto.ROTATE:
			this.editor.rotatePhoto(-1);
			break;
	}
};

SkittlesPhoto.saveResultFoto = function(){
	this.editor.getPhoto();
};

Object.defineProperties(SkittlesPhoto, {
	'preloadBarVisible': {
		set: function(value){
			if(value){
				this.$preloadBar.show();
				this.$editor.hide();
			} else {
				this.$preloadBar.hide();
				this.$editor.show();
			}
		}
	}
});