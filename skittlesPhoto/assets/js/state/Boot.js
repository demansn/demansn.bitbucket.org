SkittlesPhoto.Boot = function (game) {

};

SkittlesPhoto.Boot.prototype = {

	init: function () {

		this.input.maxPointers = 2;
		this.stage.disableVisibilityChange = true;
		this.scale.scaleMode = Phaser.ScaleManager.EXACT_FIT;
		//this.scale.setGameSize(size.max.w, size.max.h);
		//this.scale.setMinMax(size.min.w, size.min.h, size.max.w, size.max.h);
	},
	create: function () {
		this.game.state.start('Editor');
	},

};