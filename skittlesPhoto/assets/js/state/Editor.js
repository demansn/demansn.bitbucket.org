SkittlesPhoto.Editor = function() {

};

SkittlesPhoto.Editor.prototype = {
	debugText: '',
	preload: function(){
		if(SkittlesPhoto.debug){
			this.game.load.image('photo', '/assets/img/photo.jpg');
		}
	},
	init: function() {
		interact('#photoContainer', {context: this}).gesturable({
			onmove: this.onMoveGesture.bind(this)
		});

	},
	create: function() {
		if(SkittlesPhoto.debug){
			this.addPhoto();
		}

		this.game.input.addPointer();
		this.game.input.addPointer();

		SkittlesPhoto.preloadBarVisible = false;
		SkittlesPhoto.setMode(SkittlesPhoto.LOAD_PHOTO);
	},
	update: function() {


	},
	render: function() {
		if(SkittlesPhoto.debug){
			this.game.debug.pointer(this.game.input.mousePointer);
			this.game.debug.pointer(this.game.input.pointer1);
			this.game.debug.pointer(this.game.input.pointer2);
			this.game.debug.text(this.debugText, 10, 10 );
		}
	},
	disableCurrentMode: function(){

		switch(this.currentMode){
			case SkittlesPhoto.TRANSFORM:
				this.photo.inputEnabled = false;
				break;
			case SkittlesPhoto.LOAD_PHOTO:
				SkittlesPhoto.$takePhotoInput.hide();
				break;
		}
	},
	setMode: function(mode){

		this.disableCurrentMode();

		switch(mode){
			case SkittlesPhoto.TRANSFORM:
				this.photo.inputEnabled = true;
				this.photo.input.enableDrag();
				break;
			case SkittlesPhoto.LOAD_PHOTO:
				SkittlesPhoto.$takePhotoInput.show();
				break;
		}

		this.currentMode = mode;
	},
	addPhoto: function(){
		this.photo = this.game.add.sprite(this.game.world.centerX, this.game.world.centerY, 'photo');
		this.photo.anchor.setTo(0.5, 0.5);
	},
	getDistance: function(pointer){
		return Math.distance(pointer.positionDown.x, pointer.positionDown.y, pointer.position.x, pointer.position.y);
	},
	onMoveGesture: function(e){

		if(this.currentMode === SkittlesPhoto.TRANSFORM) {
			this.photo.angle += e.da;
			this.photo.scale.x += e.ds;
			this.photo.scale.y += e.ds;
		}
	},
	scalePhoto: function(ds){
		this.photo.scale.x += ds;
		this.photo.scale.y += ds;
	},
	rotatePhoto: function(da){
		this.photo.angle += da;
	},
	getPhoto: function(){
		var bmd = this.game.make.bitmapData(this.game.world.width, this.game.world.height),
			dataURL;
		
		bmd.drawFull(this.game.world);
		dataURL = bmd.canvas.toDataURL();

		//window.open(dataURL);

	/*	bmd.canvas.toBlob(function(blob) {
			saveAs(blob, "photo.png");
		});*/


		var dataURL = bmd.canvas.toDataURL( "image/png" );
		var data = atob( dataURL.substring( "data:image/png;base64,".length ) ),
		           asArray = new Uint8Array(data.length);

		 for( var i = 0, len = data.length; i < len; ++i ) {
		     asArray[i] = data.charCodeAt(i);    
		}

		var blob = new Blob( [ asArray.buffer ], {type: "image/png"} );
		saveAs(blob, "photo.png");
			/**
						var canvas = document.getElementById("canvas");
						canvas.toBlob(function(blob) {
						var newImg = document.createElement("img"),
						url = URL.createObjectURL(blob);
						newImg.onload = function() {
						// no longer need to read the blob so it's revoked
						URL.revokeObjectURL(url);
						};
						newImg.src = url;
						document.body.appendChild(newImg);
						});
			*/

		//this.game.add.image(0, 0, bmd)
	}
};


