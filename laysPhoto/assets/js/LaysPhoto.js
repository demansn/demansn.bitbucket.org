var LaysPhoto = {};
LaysPhoto.DESKTOP = 'desktop';
LaysPhoto.TABLET = 'tablet';
LaysPhoto.PHONE = 'phone';
LaysPhoto.size = {
	desktop: {
		width: 1920,
		height: 1080
	},
	tablet: {
		width:768,
		height:1024
	},
	phone: {
		width: 640,
		height: 960
	}
};

LaysPhoto.init = function(type) {

	$('.typeBtn').click(this.onClickType.bind(this));
	$('#textInput').change(this.onChangeTextInput.bind(this));

	if(window.innerWidth <= 1024){
		$('#desktop').hide();
	} else if(window.innerWidth <= 768){
		this.initRenderer('phone');
	} else {

	}

	$('.footballer').draggable({
		snap: 'canvas',
		helper: "clone",
		scroll: true,
	});

	$('#canvasContainer').droppable({
		activeClass: "active",
		hoverClass: "hover",
		drop: this.onDropDecal.bind(this)
	});

};

LaysPhoto.onClickType = function(e){

	if(this.renderer){
		return;
	}

	this.initRenderer(e.target.id);
};



LaysPhoto.initRenderer = function(type){

	var size = this.size[type]

	this.container = document.getElementById('canvasContainer');
	this.renderer = PIXI.autoDetectRenderer(size.width, size.height, {backgroundColor : 0x1099bb});
	this.container.appendChild(this.renderer.view);
	this.stage = new PIXI.Container();
	this.stage.width = size.width;
	this.stage.height = size.height;
	this.elementsContainer = new PIXI.Container();
	this.stage.addChild(this.elementsContainer);

	this.transformController = new LaysPhoto.TransformController();
	this.stage.addChild(this.transformController);

	this.transformController.visible = false;

	this.stage.interactive = true;
	this.stage
		.on('mousedown', this.inputOnDownEdit, this)
		.on('touchstart', this.inputOnDownEdit, this)


	this.render();

	$('#typeConatiner').hide();
	$('#editor').show();
};

LaysPhoto.onChangeTextInput = function(e){
	this.addText(e.target.value);
};


LaysPhoto.addText = function(text){

	if(text.length > 0){
		if(!this.text){
			this.text = new PIXI.Text('aa', { font: '100px Areal', fill: 'white', align: 'center' });
			this.text.position.set(this.renderer.width / 2, this.renderer.height / 2);
			this.text.ID = 'OBJ';
	/*		this.text.interactive = true;
			this.text.tap = this.onSelectText.bind(this);
			this.text.click = this.onSelectText.bind(this);*/
			this.elementsContainer.addChild(this.text);
		}

		this.text.text = text;
	} else {
		if(this.text){
			this.elementsContainer.removeChild(this.text);
			this.text = null;
		}
	}

};

LaysPhoto.onDropDecal = function(e, ui){

	var x = this.renderer.width / 100 * (100 * (e.clientX - e.offsetX - e.target.offsetLeft) / this.renderer.view.clientWidth),
		y = this.renderer.height / 100 * (100 * (e.clientY - e.offsetY - e.target.offsetTop) / this.renderer.view.clientHeight)

	this.addDecal(e.toElement.id, x, y);
};

LaysPhoto.addDecal = function(id, x, y){

	var texture = PIXI.Texture.fromImage('assets/img/' + id + '.png'),
		decal = new PIXI.Sprite(texture);

	decal.position.set(x, y);
/*	decal.interactive = true;
	decal.tap = this.onSelectDecal.bind(this);
	decal.click = this.onSelectDecal.bind(this);*/
	decal.ID = 'OBJ';

	this.elementsContainer.addChild(decal);

};

LaysPhoto.onSelectDecal = function(e){
	this.selectObject(e.target);
};

LaysPhoto.inputOnDownEdit = function(e){

	var i = 0,
		child = null;

	for(i = 0; i < this.elementsContainer.children.length; i += 1){
		child = this.elementsContainer.children[i];
		if(this.isIntersecting(e.data.global.x, e.data.global.y, child)){
			this.selectObject(child);
			return;
		}
	}

/*	if(this.transformController.visible && !this.isIntersecting(e.data.global.x, e.data.global.y, this.transformController)){
		this.unselectObject();
	}*/

};

LaysPhoto.isIntersecting = function(x, y, obj) {
	return !(obj.x > x || (obj.x + obj.width) < x || obj.y > y || (obj.y + obj.height) < y);
};

LaysPhoto.selectObject = function(object){
	this.transformController.setObject(object);
	this.transformController.visible = true;
};

LaysPhoto.unselectObject = function(){
	this.transformController.visible = false;
};

LaysPhoto.render = function() {
	requestAnimationFrame(LaysPhoto.render.bind(this));
	this.renderer.render(this.stage);
};