LaysPhoto.TransformController = function(){

	PIXI.Container.call(this);

	this.border = new PIXI.Graphics();
	
	this.oldPosition = new PIXI.Point(0, 0);
	this.addChild(this.border);

	this.interactive = true;
	this
		.on('mousedown', this.inputOnDown, this)
		.on('touchstart', this.inputOnDown, this)
		.on('mouseup', this.inputOnUp, this)
		.on('touchend', this.inputOnUp, this)
		.on('mousemove', this.inputOnMove, this)
		.on('touchmove', this.inputOnMove, this)
		.on('mouseupoutside', this.inputOnUp, this)
		.on('touchendoutside', this.inputOnUp, this)

	var size = 50,
		color = 0x000000;

	this.leftTop = this.createEdit('leftTop', size, color);
	this.leftBottom = this.createEdit('leftBottom', size, color);
	this.leftCenter = this.createEdit('leftCenter', size, color);
	this.rightTop = this.createEdit('rightTop', size, color);
	this.rightBottom = this.createEdit('rightBottom', size, color);
	this.rightCenter = this.createEdit('rightCenter', size, color);
	this.bottom = this.createEdit('bottom', size, color);
	this.top = this.createEdit('top', size, color);

	this.rect = new PIXI.Rectangle(0,0,1,1);

};

LaysPhoto.TransformController.prototype = Object.create(PIXI.Container.prototype);
LaysPhoto.TransformController.constructor = LaysPhoto.TransformController;

LaysPhoto.TransformController.prototype.createEdit = function(id, size, color) {

	var graphics = new PIXI.Graphics(),
		container = new PIXI.Container();

	graphics.width = size;
	graphics.height = size;
	graphics.beginFill(color, 1);
	graphics.drawRect(-size / 2, -size / 2, size, size);
	graphics.endFill();

	container.addChild(graphics);

	container.ID = id;
	container.interactive = true;
	container.hitArea = new PIXI.Rectangle(-size / 2, -size / 2, size, size);
	container
		.on('mousedown', this.inputOnDownEdit, this)
		.on('touchstart', this.inputOnDownEdit, this)
		.on('mouseup', this.inputOnUpEdit, this)
		.on('touchend', this.inputOnUpEdit, this)
		.on('mousemove', this.inputOnMoveEdit, this)
		.on('touchmove', this.inputOnMoveEdit, this)
		.on('mouseupoutside', this.inputOnUpEdit, this)
		.on('touchendoutside', this.inputOnUpEdit, this)
		.on('mouseover', this.inputOnOverEdit, this)
		.on('mouseout', this.inputOnOutEdit, this)

	this.addChild(container);

	return container;
};

LaysPhoto.TransformController.prototype.inputOnDown = function(e) {
	this.isDown = true;
};

LaysPhoto.TransformController.prototype.inputOnUp = function(e) {
	this.isDown = false;
	if(!this.isDownEdit){
		this.oldPosition.set(0, 0);
	}
};

LaysPhoto.TransformController.prototype.inputOnMove = function(e) {

	if(this.isDown && !this.isDownEdit){

		if(this.oldPosition.x == 0 || this.oldPosition.y == 0){
			this.oldPosition.set(e.data.global.x, e.data.global.y);
		}

		this.rect.x += e.data.global.x - this.oldPosition.x;
		this.rect.y += e.data.global.y - this.oldPosition.y;;
		this.update();
		this.oldPosition.set(e.data.global.x, e.data.global.y);
	}

	console.log('move ---');

};

LaysPhoto.TransformController.prototype.inputOnDownEdit = function(e) {
	this.isDownEdit = e.target.ID;
	this.oldPosition.set(e.data.global.x, e.data.global.y);
	console.log('down ' + this.isDownEdit);
};

LaysPhoto.TransformController.prototype.inputOnUpEdit = function(e) {
	console.log('up ' + this.isDownEdit);
	this.isDownEdit = null;

};

LaysPhoto.TransformController.prototype.inputOnMoveEdit = function(e) {

	var dx = e.data.global.x - this.oldPosition.x,
		dy = e.data.global.y - this.oldPosition.y,
		deltaSize = 0;
		deltaSize += Math.max(dx, dy);

	if(this.isDownEdit && this.isDownEdit == e.target.ID){

		var dx = e.data.global.x - this.oldPosition.x,
			dy = e.data.global.y - this.oldPosition.y,
			deltaSize = 0;

		switch(this.isDownEdit){
			case 'leftTop':
				if(e.data.global.x <= this.oldPosition.x && e.data.global.y <= this.oldPosition.y){
					deltaSize = Math.min(dx, dy);
				} else {
					deltaSize = Math.max(dx, dy);
				}

				this.rect.x += deltaSize / 2;
				this.rect.y += deltaSize / 2;
				this.rect.width -= deltaSize / 2;
				this.rect.height -= deltaSize / 2;

				break;
			case 'leftBottom':
				if(e.data.global.x <= this.oldPosition.x && e.data.global.y >= this.oldPosition.y){
					deltaSize = Math.min(dx, dy);
				} else {
					deltaSize = Math.max(dx, dy);
				}

				this.rect.x += deltaSize ;
				this.rect.width -= deltaSize;
				this.rect.height -= deltaSize;
				break;
			case 'leftCenter':
				deltaSize = dx;
				this.rect.x += deltaSize;
				this.rect.width -= deltaSize;
				break;
			case 'rightTop':
				if(e.data.global.x >= this.oldPosition.x && e.data.global.y <= this.oldPosition.y){
					deltaSize = Math.max(dx, dy);
				} else {
					deltaSize = Math.min(dx, dy);
				}

				this.rect.y -= deltaSize;
				this.rect.width += deltaSize;
				this.rect.height += deltaSize;

				break;
			case 'rightBottom':
				if(e.data.global.x >= this.oldPosition.x && e.data.global.y >= this.oldPosition.y){
					deltaSize = Math.max(dx, dy);
				} else {
					deltaSize = Math.min(dx, dy);
				}

				this.rect.width += deltaSize / 2;
				this.rect.height += deltaSize / 2;
				break;
			case 'rightCenter':
				deltaSize = dx;
				this.rect.width += deltaSize;
				break;
			case 'bottom':
				deltaSize = dy;
				this.rect.height += deltaSize;
				break;
			case 'top':
				deltaSize = dy;
				this.rect.y += deltaSize;
				this.rect.height -= deltaSize;
				break;
			default:
				
				break;
		}
		this.update();
		this.oldPosition.set(e.data.global.x, e.data.global.y);
	}

};

LaysPhoto.TransformController.prototype.inputOnOverEdit = function(e) {

	switch(e.target.ID){
		case 'leftTop':
			document.body.style.cursor = 'nw-resize';
			break;
		case 'leftBottom':
			document.body.style.cursor = 'sw-resize';
			break;
		case 'leftCenter':
			document.body.style.cursor = 'w-resize';
			break;
		case 'rightTop':
			document.body.style.cursor = 'ne-resize';
			break;
		case 'rightBottom':
			document.body.style.cursor = 'se-resize';
			break;
		case 'rightCenter':
			document.body.style.cursor = 'e-resize';
			break;
		case 'bottom':
			document.body.style.cursor = 's-resize';
			break;
		case 'top':
			document.body.style.cursor = 'n-resize';
			break;
		default:
			document.body.style.cursor = '';
			break;
	}

};

LaysPhoto.TransformController.prototype.inputOnOutEdit = function(e) {
	document.body.style.cursor = '';
};

LaysPhoto.TransformController.prototype.setObject = function(object) {
	this.object = object;
	this.rect.x = object.x;
	this.rect.y = object.y;
	this.rect.width = object.width;
	this.rect.height = object.height;
	this.update();
};

LaysPhoto.TransformController.prototype.updateBorder = function(object) {
	this.border.clear();
	this.border.lineStyle(10, 0xFFFFFF, 1);
	this.border.beginFill(0xFF700B, 0);
	this.border.drawRect(0, 0, this.rect.width, this.rect.height);
	this.border.endFill();
};

LaysPhoto.TransformController.prototype.updateObjectSize = function() {
	this.object.x = this.rect.x;
	this.object.y = this.rect.y;

	this.object.width = this.rect.width;
	this.object.height = this.rect.height;
};

LaysPhoto.TransformController.prototype.update = function() {

	this.x = this.rect.x;
	this.y = this.rect.y;

	this.leftBottom.y = this.rect.height;
	this.leftCenter.y = this.rect.height / 2;

	this.rightTop.x = this.rect.width;
	this.rightCenter.x = this.rect.width;
	this.rightBottom.x = this.rect.width;

	this.rightBottom.y = this.rect.height;
	this.rightCenter.y = this.rect.height / 2;

	this.bottom.x = this.rect.width / 2;
	this.bottom.y = this.rect.height;
	this.top.x = this.rect.width / 2;

	this.updateObjectSize();
	this.updateBorder();
};

