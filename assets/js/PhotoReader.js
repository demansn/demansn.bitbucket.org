SkittlesPhoto.PhotoReader = function() {

	EventEmitter.call(this);

	if(!window.FileReader){
		alert('Ваш браузре устарел!');
		return false;
	}

	this.progress = 0;
};
SkittlesPhoto.PhotoReader.prototype = Object.create(EventEmitter.prototype);
SkittlesPhoto.PhotoReader.constructor = SkittlesPhoto.PhotoReader;

SkittlesPhoto.PhotoReader.prototype._onError = function(e) {

	var error = {message:''};

	switch(e.target.error.code) {
		case e.target.error.NOT_FOUND_ERR:
			error.message = 'File Not Found!';
			break;
		case e.target.error.NOT_READABLE_ERR:
			error.message = 'File is not readable';
			break;
		case e.target.error.ABORT_ERR:
			break; // noop
		default:
			error.message = 'An error occurred reading this file.';
	}

	this.emit('onError', e);
};

SkittlesPhoto.PhotoReader.prototype._onProgress = function(e) {

	var percentLoaded = 0;

	if (e.lengthComputable) {
		percentLoaded = Math.round((e.loaded / e.total) * 100);
		// Increase the progress bar length.
		if (percentLoaded < 100) {
			this.progress = percentLoaded;
		}
	}
};

SkittlesPhoto.PhotoReader.prototype._onLoad = function(e) {
	this.progress = 100;
	this.emit('onLoad', e);
};

SkittlesPhoto.PhotoReader.prototype._onLoadStart = function(e) {
	this.progress = 0;
	this.emit('onLoadStart', e);
};

SkittlesPhoto.PhotoReader.prototype._onAbort = function(e) {
	this.emit('onAbort', e);
};

SkittlesPhoto.PhotoReader.prototype.loadPhoto = function(file) {

	this._fileReader = new FileReader();
	this._fileReader.onerror = this._onError.bind(this);
	this._fileReader.onprogress = this._onProgress.bind(this);
	this._fileReader.onabort = this._onAbort.bind(this);
	this._fileReader.onloadstart = this._onLoadStart.bind(this);
	this._fileReader.onload = this._onLoad.bind(this);

	this._fileReader.readAsDataURL(file);
};

SkittlesPhoto.PhotoReader.prototype.abortLoad = function() {
	this._fileReader.abort();
};

Object.defineProperty(SkittlesPhoto.PhotoReader.prototype, 'progress', {
	set: function(value) {
		this._progress = value;
		this.emit('onProgress', {'progress': this._progress});
	},
	get: function(){
		return this._progress;
	}
});