LaysPhoto.TransformController = function(){

	PIXI.Container.call(this);

	this.border = new PIXI.Graphics();
	
	this.oldPosition = new PIXI.Point(0, 0);
	this.addChild(this.border);

	this.interactive = true;
	this
		.on('mousedown', this.inputOnDown, this)
		.on('touchstart', this.inputOnDown, this)
		.on('mouseup', this.inputOnUp, this)
		.on('touchend', this.inputOnUp, this)
		.on('mousemove', this.inputOnMove, this)
		.on('touchmove', this.inputOnMove, this)
		.on('mouseupoutside', this.inputOnUp, this)
		.on('touchendoutside', this.inputOnUp, this)

	this.leftTop = new PIXI.Graphics();
	this.leftTop.beginFill(0xFF700B, 1);
	this.leftTop.drawRect(-25, -25, 25, 25);
	this.leftTop.endFill();
	this.leftTop.ID = 'leftTop';
	this.initEventsEdit(this.leftTop);
	this.addChild(this.leftTop);

	this.leftBottom = new PIXI.Graphics();
	this.leftBottom.beginFill(0xFF700B, 1);
	this.leftBottom.drawRect(-25, -25, 25, 25);
	this.leftBottom.endFill();
	this.leftBottom.ID = 'leftBottom';
	this.initEventsEdit(this.leftBottom);
	this.addChild(this.leftBottom);

};

LaysPhoto.TransformController.prototype = Object.create(PIXI.Container.prototype);
LaysPhoto.TransformController.constructor = LaysPhoto.TransformController;

LaysPhoto.TransformController.prototype.initEventsEdit = function(edit) {
	edit.interactive = true;
	edit
		.on('mousedown', this.inputOnDownEdit, this)
		.on('touchstart', this.inputOnDownEdit, this)
		.on('mouseup', this.inputOnUpEdit, this)
		.on('touchend', this.inputOnUpEdit, this)
		.on('mousemove', this.inputOnMoveEdit, this)
		.on('touchmove', this.inputOnMoveEdit, this)
		.on('mouseupoutside', this.inputOnUpEdit, this)
		.on('touchendoutside', this.inputOnUpEdit, this)
		.on('mouseover', this.inputOnOverEdit, this)
		.on('mouseout', this.inputOnOutEdit, this)
};

LaysPhoto.TransformController.prototype.inputOnDown = function(e) {
	this.isDown = true;
};

LaysPhoto.TransformController.prototype.inputOnUp = function(e) {
	this.isDown = false;
	this.oldPosition.set(0, 0);
};

LaysPhoto.TransformController.prototype.inputOnMove = function(e) {

	if(this.isDown){

		if(this.oldPosition.x == 0 || this.oldPosition.y == 0){
			this.oldPosition.set(e.data.global.x, e.data.global.y);
		}

		this.object.x += e.data.global.x - this.oldPosition.x;
		this.object.y += e.data.global.y - this.oldPosition.y;;
		this.x = this.object.x;
		this.y = this.object.y;
		this.oldPosition.set(e.data.global.x, e.data.global.y);
	}

};

LaysPhoto.TransformController.prototype.inputOnDownEdit = function(e) {
	this.isDownEdit = e.target.ID;
};

LaysPhoto.TransformController.prototype.inputOnUpEdit = function(e) {
	this.isDownEdit = null;
};

LaysPhoto.TransformController.prototype.inputOnMoveEdit = function(e) {



};

LaysPhoto.TransformController.prototype.inputOnOverEdit = function(e) {

	switch(e.target.ID){
		case 'leftTop':
			document.body.style.cursor = 'nw-resize';
			break;
		case 'leftBottom':
			document.body.style.cursor = 'nw-resize';
			break;
		case 'leftCenter':
			document.body.style.cursor = 'nw-resize';
			break;
		case 'rightTop':
			document.body.style.cursor = 'nw-resize';
			break;
		case 'rightBottom':
			document.body.style.cursor = 'nw-resize';
			break;
		case 'rightCenter':
			document.body.style.cursor = 'nw-resize';
			break;
		case 'bottom':
			document.body.style.cursor = 'nw-resize';
			break;
		case 'top':
			document.body.style.cursor = 'nw-resize';
			break;
		default:
			document.body.style.cursor = '';
			break;
	}

};

LaysPhoto.TransformController.prototype.inputOnOutEdit = function(e) {
	document.body.style.cursor = '';
};

LaysPhoto.TransformController.prototype.setObject = function(object) {
	this.object = object;
	this.x = object.x;
	this.y = object.y;
	this.update();
};

LaysPhoto.TransformController.prototype.updateBorder = function(object) {


	this.border.clear();
	this.border.lineStyle(10, 0xFFFFFF, 1);
	this.border.beginFill(0xFF700B, 0);
	this.border.drawRect(0, 0, this.object.width, this.object.height);
	this.border.endFill();
};

LaysPhoto.TransformController.prototype.update = function() {

	this.updateBorder();
};

