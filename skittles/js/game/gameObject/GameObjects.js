SkittlesGame.GameObjects = function(game){
	Phaser.Group.call(this, game);

	this.enableBody = true;
	this.physicsBodyType = Phaser.Physics.ARCADE;

	this.createMultiple(10, 'skittles', 0, false);
	this.createMultiple(6, 'brunch', 0, false);
	this.createMultiple(6, 'barrier', 0, false);

	this.setAll('body.velocity.y', 300);
	this.setAll('body.immovable', true);
	this.setAll('anchor.x', 0.5);

	this._columnWidht = this.game.world.width / 5;
};


SkittlesGame.GameObjects.prototype = Object.create(Phaser.Group.prototype);
SkittlesGame.GameObjects.constructor = SkittlesGame.GameObjects;

SkittlesGame.GameObjects.prototype.startGenerate = function() {
	this._ev1 = this.game.time.events.loop(Phaser.Timer.SECOND / 2, this._addSkittles, this);
	this._ev2 = this.game.time.events.loop(Phaser.Timer.SECOND, this._addBrunch, this);
	this._ev3 = this.game.time.events.loop(Phaser.Timer.SECOND, this._addBarrier, this);
};

SkittlesGame.GameObjects.prototype._addSkittles = function() {

	var item = this.getFirstDead(false, this.getRundomColumnPosition(1, 5), 0, 'skittles');

	if (item) {
		item.y = -item.height + 1;
		item.eaten = false;

		item.body.velocity.y = 300;
		item.body.immovable = true;
	}
};

SkittlesGame.GameObjects.prototype._addBrunch = function() {

	var item = this.getFirstDead(false, this.getPickColumnPosition([1, 5]), 0, 'brunch');

	if (item) {
		item.y = -item.height + 1;

		item.body.velocity.y = 300;
		item.body.immovable = true;
	}
};

SkittlesGame.GameObjects.prototype._addBarrier = function() {

	var item = this.getFirstDead(false, this.getRundomColumnPosition(2, 4), 0, 'barrier');

	if (item) {
		item.y = -item.height + 1;

		item.body.velocity.y = 300;
		item.body.immovable = true;
	}
};

SkittlesGame.GameObjects.prototype.stopAll = function() {

	this.setAll('body.velocity.y', 0);

	this.game.time.events.remove(this._ev1);
	this.game.time.events.remove(this._ev2);
	this.game.time.events.remove(this._ev3);
};

SkittlesGame.GameObjects.prototype.getRundomColumnPosition = function(min, max) {

	var columnNumber = this.game.rnd.integerInRange(min, max),
		columnPos = (this._columnWidht * columnNumber) - this._columnWidht / 2;

	return columnPos;
};

SkittlesGame.GameObjects.prototype.getPickColumnPosition = function(positions) {

	var columnNumber = this.game.rnd.pick(positions),
		columnPos = (this._columnWidht * columnNumber) - this._columnWidht / 2;

	return columnPos;
};

SkittlesGame.GameObjects.prototype.killAll = function() {
	this.forEachAlive(function(go){
		go.kill();
	});
};

SkittlesGame.GameObjects.prototype.update = function() {

	this.forEach(function(go){
		if(!go.inCamera){
			go.kill();
		}
	});
};

