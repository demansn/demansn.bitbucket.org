SkittlesGame.Hero = function(game) {

	Phaser.Sprite.call(this, game, game.world.width / 2, game.world.height - 20, 'hero');

	this.anchor.setTo(0.5, 1);
	this.directionMove = 0;
	this.velocityMove = 300;

	this.game.physics.enable(this, Phaser.Physics.ARCADE);
	this.body.collideWorldBounds = true;
	this.body.immovable = true;
	this.game.add.existing(this);
};

SkittlesGame.Hero.prototype = Object.create(Phaser.Sprite.prototype);
SkittlesGame.Hero.constructor = SkittlesGame.Hero;

SkittlesGame.Hero.prototype.startMove = function(){
	//this.body.velocity.y = -300;
};

SkittlesGame.Hero.prototype.reset = function(){
	this.x = this.game.world.width / 2;
};

SkittlesGame.Hero.prototype.ateSkitls = function(skitls){
	this.currentSkitls = skitls;
	this.currentSkitls.kill();
	SkittlesGame.score += 1;
};

SkittlesGame.Hero.prototype.update = function(){

	this.body.velocity.x = this.velocityMove * this.directionMove;

};