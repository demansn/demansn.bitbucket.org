SkittlesGame.GameHUD = function(game){

	Phaser.Group.call(this, game);

	this.game = game;

	this.buttonLeft = this.create(0, 0, 'btnLeft');
	this.buttonLeft.inputEnabled = true;
	this.buttonLeft.events.onInputDown.add(this._onDownLeftBtn, this);
	this.buttonLeft.events.onInputUp.add(this._onUpLeftBtn, this);
	this.buttonLeft.fixedToCamera = true;
	this.buttonLeft.cameraOffset.set(15, this.game.camera.height - 15);
	this.buttonLeft.anchor.set(0, 1);

	this.buttonRight = this.create(0, 0, 'btnRight');
	this.buttonRight.inputEnabled = true;
	this.buttonRight.events.onInputDown.add(this._onDownRightBtn, this);
	this.buttonRight.events.onInputUp.add(this._onUpRightBtn, this);
	this.buttonRight.fixedToCamera = true;
	this.buttonRight.cameraOffset.set(this.game.camera.width - 15, this.game.camera.height - 15);
	this.buttonRight.anchor.set(1, 1);

	var style = { font: "bold 32px Arial", fill: "#fff", boundsAlignH: "center", boundsAlignV: "middle" };

	//  The Text is positioned at 0, 100
	this.scoreView = this.game.add.text(0, 0, "0", style);
	this.scoreView.anchor.x = 0.5;
	this.scoreView.fixedToCamera = true;
	this.scoreView.cameraOffset.set(this.game.camera.width / 2, 10);

};

SkittlesGame.GameHUD.prototype = Object.create(Phaser.Group.prototype);
SkittlesGame.GameHUD.constructor = SkittlesGame.GameHUD;

SkittlesGame.GameHUD.prototype._onDownLeftBtn = function() {
	this.isPressLeft = true;
};

SkittlesGame.GameHUD.prototype._onUpLeftBtn = function() {
	this.isPressLeft = false;
};

SkittlesGame.GameHUD.prototype._onDownRightBtn = function() {
	this.isPressRight = true;
};

SkittlesGame.GameHUD.prototype._onUpRightBtn = function() {
	this.isPressRight = false;
};

SkittlesGame.GameHUD.prototype.update = function() {
	this.scoreView.setText(SkittlesGame.score);
};
