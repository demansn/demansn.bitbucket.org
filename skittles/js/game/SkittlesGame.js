var SkittlesGame = {
	score: 0,
	music: null,
	orientated: false
};


SkittlesGame.Boot = function (game) {

};

SkittlesGame.Boot.prototype = {

	init: function () {

		this.input.maxPointers = 1;
		this.stage.disableVisibilityChange = true;

		if (this.game.device.desktop) {
			this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
			this.scale.setMinMax(500, 500, 1000, 1000);
			this.scale.pageAlignHorizontally = true;
			this.scale.pageAlignVertically = true;
		} else {
			this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
			this.scale.setMinMax(200, 200, 1000, 1000);
			this.scale.pageAlignHorizontally = true;
			this.scale.pageAlignVertically = true;
			this.scale.forceOrientation(true, false);
			this.scale.setResizeCallback(this.gameResized, this);
			this.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
			this.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);
		}

	},

	preload: function () {
		this.load.image('preloaderBackground', 'img/preloadBarBgr.png');
		this.load.image('preloaderBar', 'img/preloadBarFull.png');
	},

	create: function () {
		this.state.start('Preloader');
	},

	gameResized: function (width, height) {

		//  This could be handy if you need to do any extra processing if the game resizes.
		//  A resize could happen if for example swapping orientation on a device or resizing the browser window.
		//  Note that this callback is only really useful if you use a ScaleMode of RESIZE and place it inside your main game state.

	},

	enterIncorrectOrientation: function () {

		SkittlesGame.orientated = false;

		document.getElementById('orientation').style.display = 'block';

	},

	leaveIncorrectOrientation: function () {

		SkittlesGame.orientated = true;

		document.getElementById('orientation').style.display = 'none';

	}

};