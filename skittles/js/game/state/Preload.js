SkittlesGame.Preloader = function(game) {
	this.background = null;
	this.preloadBar = null;

	this.ready = false;
};


SkittlesGame.Preloader.prototype = {

	preload: function () {

		this.background = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'preloaderBackground');
		this.background.x -= this.background.width / 2;
		this.background.anchor.setTo(0, 0.5);

		this.preloadBar = this.add.sprite(this.game.world.centerX, this.game.world.centerY, 'preloaderBar');
		this.preloadBar.x -= this.preloadBar.width / 2;
		this.preloadBar.anchor.setTo(0, 0.5);

		this.load.setPreloadSprite(this.preloadBar);

		//загрузка мусора для демонстрации прогресс бара загрузки
		for(var i = 1; i < 18; i+= 1){
			this.load.image(i, 'img/trash/' + i + '.jpg');
		}

		this.load.image('hero', 'img/hero.png');
		this.load.image('btnLeft', 'img/btnLeft.png');
		this.load.image('btnRight', 'img/btnRight.png');
		this.load.image('skittles', 'img/skittles.png');
		this.load.image('brunch', 'img/brunch.png');
		this.load.image('barrier', 'img/barrier.png');
		this.load.image('background', 'img/background.png');

	},

	create: function () {

		this.preloadBar.cropEnabled = false;

	},
	update: function () {

	/*	if (this.cache.isSoundDecoded('titleMusic') && this.ready == false) {
			this.ready = true;
			this.state.start('MainMenu');
		}*/
		this.state.start('Game');

	}

};