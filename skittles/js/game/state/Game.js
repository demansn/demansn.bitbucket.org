SkittlesGame.Game = function() {

};

SkittlesGame.Game.prototype = {
	create: function() {

		//this.world.setBounds(1000, 1000);

		this.game.physics.startSystem(Phaser.Physics.ARCADE);
		this.background = this.game.add.tileSprite(0, 0, 1000, 2000, 'background');
		this.hero = new SkittlesGame.Hero(this.game);
		this.objects = new SkittlesGame.GameObjects(this.game);
		this.hud = new SkittlesGame.GameHUD(this.game);

		this.play();
	},

	update: function() {

		if(this.isPlay){

			this.hero.directionMove = 0;

			if(this.game.input.keyboard.isDown(Phaser.Keyboard.LEFT) || this.hud.isPressLeft) {
				this.hero.directionMove = -1;
			}

			if(this.game.input.keyboard.isDown(Phaser.Keyboard.RIGHT) || this.hud.isPressRight) {
				this.hero.directionMove = 1;
			}

			this.hero.update();
			this.objects.update();

			this.background.tilePosition.y += 2;

			if (this.game.physics.arcade.overlap(this.hero, this.objects, this.collisionHandler, this.processHandler, this)) {
				console.log('boom');
			}

		}
	},
	play: function() {

		this.hero.startMove();
		this.objects.startGenerate();
		this.isPlay = true;

	},
	gameOver: function() {
		this.objects.stopAll();
		this.reset();
	},
	reset: function() {

		SkittlesGame.score = 0;
		this.hero.reset();
		this.objects.killAll();
		this.play();
	},
	processHandler: function (player, veg) {
		return true;
	},
	collisionHandler: function(hero, gameObject) {
		if (gameObject.key == 'skittles' && !gameObject.eaten) {

			gameObject.eaten = true;
			this.hero.ateSkitls(gameObject);

		} else if(gameObject.key == 'brunch' || gameObject.key == 'barrier'){
			this.gameOver();
		}
	}
};